/*================================================================================*/
/* MOBILE SETTINGS */
/*================================================================================*/

var onMobile = false;

/* Clears the use_mobile cookie. */
function clearPreferences() {
	document.cookie = "use_mobile=; expires=Thu, 01 Jan 1970 00:00:00 GMT";
	alert("Removed preferences cookie.");
}

/* Get the Value of a Cookie */	
function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i].trim();
		if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	}
	return "";
} 

/* Check if user is on a mobile device and set CSS file */
function selectFittingCSS() {
	var fileref = document.createElement("link");
	fileref.setAttribute("rel", "stylesheet");
	fileref.setAttribute("type", "text/css");

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		var preference = getCookie("use_mobile");
		if (preference == "") {
			var useMobile = confirm("Hey there! You are using a mobile device. Do you want to be redirected to the mobile version of this website?");
			if (useMobile == true) {
				fileref.setAttribute("href", "css/style_mobile.css");
                document.cookie="use_mobile=yes";
                onMobile = true;
			} else {
				fileref.setAttribute("href", "css/style.css");
                document.cookie="use_mobile=no";
                onMobile = false;
			}
		} else if (preference == "yes") {
            fileref.setAttribute("href", "css/style_mobile.css");
            onMobile = true;
		} else {
            fileref.setAttribute("href", "css/style.css");
            onMobile = false;					
		}
	} else {
        fileref.setAttribute("href", "css/style.css");
        onMobile = false;
	}

	document.getElementsByTagName("head")[0].appendChild(fileref);
}









/*================================================================================*/
/* SIDENAV LEFT */
/*================================================================================*/

var menueOpenLeft = false;
var checkboxAudio = false;
var checkboxNormal = true;
var scrollSpeed = 0;
var volumeThreshold = 0.000001;
var numPressed = 0;

/* Open the Left side Menu */
function openNavLeft() {
	menueOpenLeft = true;
	if(onMobile){
		if(menueOpenLeft) {
			closeNavRight();
	   }
		document.getElementById("sidenavLeft").style.width = "100%";
	} else {
		document.getElementById("sidenavLeft").style.width = "250px";
		document.getElementById("main").style.marginLeft = "250px";
	}
}

/* Close the Left side Menu */
function closeNavLeft() {
	menueOpenLeft = false;
	document.getElementById("sidenavLeft").style.width = "0";
	document.getElementById("main").style.marginLeft = "0";
}

/* Opens and Closes the Left menu if clicked on the Left Menue Icon */
function clickMenueIconLeft(x) {
    x.classList.toggle("change");
    if(menueOpenLeft) {
        closeNavLeft();
    } else {
        openNavLeft();
    }
}

/* activate / deactivate the Scroll on Sound Checkbox */
function toggleAudioCheckbox() {
	var checkbox = document.getElementById("audio");
	var settings = document.getElementById("audioSetting");
	if(checkbox.checked == true) {
		checkbox.checked = false;
		checkboxAudio = false;
		settings.style.display = "none";
	} else {
		checkbox.checked = true;
		checkboxAudio = true;
		settings.style.display = "block"
		if(checkboxNormal){
			toggleNormalCheckbox();
		}
	}
}

/* Function that is executed if klicked on the sroll on Sound Checkbox */
function clickAudioCheckbox() {
	checkboxAudio = document.getElementById("audio").checked;
	var settings = document.getElementById("audioSetting");
	if(!checkboxAudio) {
		settings.style.display = "none";
	} else {
		settings.style.display = "block"
	}
	if(checkboxAudio && checkboxNormal) {
		toggleNormalCheckbox();
	}
}

/* activate / deactivate the Scroll normaly Checkbox */
function toggleNormalCheckbox() {
	var checkbox = document.getElementById("normal");;
	var settings = document.getElementById("normalSettings");
	if(checkbox.checked) {
		checkbox.checked = false;
		checkboxNormal = false;
		settings.style.display = "none";
		stopScroll();
	} else {
		checkbox.checked = true;
		checkboxNormal = true;
		settings.style.display = "block"
		if(checkboxAudio) {
			toggleAudioCheckbox();
		}
	}
}

/* Function that is executed if klicked on the sroll normal Checkbox */
function clickNormalCheckbox() {
	checkboxNormal = document.getElementById("normal").checked;
	var settings = document.getElementById("normalSettings");
	if(!checkboxNormal) {
		stopScroll();
		settings.style.display = "none";
	} else {
		settings.style.display = "block"
	}
	if(checkboxAudio && checkboxNormal) {
		toggleAudioCheckbox();
	}
}

/* Set the Threshold for the soudn Input with the Threshold Slider*/
function setThreshold(){
	var slider = document.getElementById("soundSlider");
	volumeThreshold = slider.value * 0.000001;
}

/* Print the Slider Value of The Scroll Speed Slider */
function printSliderValue() {
	var output = document.getElementById('sliderOutput');
	output.innerHTML = scrollSpeed;
}

/* Set the Value of the Scroll Speed Slider */
function setSliderValue() {
	var slider = document.getElementById('scrollSlider');
	scrollSpeed = slider.value;
	printSliderValue();
}

/* Countdown with the Snackbar from 5 */
function snackbarCountdown() {
	var x = document.getElementById("snackbar");
	x.innerHTML = 5;
	x.className = "show";
	setTimeout(function(){x.innerHTML = 4;},1000);
	setTimeout(function(){x.innerHTML = 3;},2000);
	setTimeout(function(){x.innerHTML = 2;},3000);
	setTimeout(function(){x.innerHTML = 1;},4000);
	setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5500);
}









/*================================================================================*/
/* SIDENAV RIGHT */
/*================================================================================*/

var menueOpenRight = false;

/* Open the Right side Menu */
function openNavRigth() {
	menueOpenRight = true;
	if(onMobile) {
		if(menueOpenLeft) {
			 closeNavLeft();
		}
		document.getElementById("sidenavRight").style.width = "100%";
	} else {
		document.getElementById("sidenavRight").style.width = "350px";
	}
}

/* Close the Right side Menu */
function closeNavRight() {
	menueOpenRight = false;
	document.getElementById("sidenavRight").style.width = "0";
}

/* Opens and Closes the Right menu if clicked on the Right Menue Icon */
function clickMenueIconRight(x) {
	x.classList.toggle("change");
    if(menueOpenRight) {
        closeNavRight();
    } else {
        openNavRigth();
    }
}

/* Open the Right side Menue if clicked on the Select a Song button */
function selectASong() {
	openNavRigth();
	document.getElementById("menueIconContainerRight").classList.toggle("change");
	document.getElementById("selectASongButton").style.display = "none";
}

/* Load a selected Song */
function loadSong(songName, caller) {
	var songTitle = document.getElementById("songName");
	var x = document.getElementById("snackbar");
	x.innerHTML = "Loading...";
	x.className = "show";
   $("#tabs").load("tabs/" + songName + ".txt", function(responseTxt, statusTxt, xhr){
        if(statusTxt == "success") {
			console.log("Loaded" + caller.innerHTML);
			songTitle.innerHTML = caller.innerHTML;
			document.body.scrollTop = document.documentElement.scrollTop = 0;
			x.className = x.className.replace("show", "");
			closeNavRight();
		}
        if(statusTxt == "error") {
			alert("Sorry, your song could not be loaded.");
			console.log("Error: " + xhr.status + ": " + xhr.statusText);
			x.className = x.className.replace("show", "");
		}
	});
}









/*================================================================================*/
/* MOBILE MENUE */
/*================================================================================*/

/* Open the Dropdown mobile on Mobile */
function showMobileDropdown() {
    document.getElementById("dropDownMenue").classList.toggle("show");
}

/* Close the dropdown menu if the user clicks outside of it */
window.onclick = function(event) {
  if (!event.target.matches('.dropdownMenuIcon')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}









/*================================================================================*/
/* SCROLL STUFF */
/*================================================================================*/

var scrolldelay;

/* When the user scrolls the page, trigger a scrollEvent */ 
window.onscroll = function() {
	scrollEvent()
};

/* Sets the progress-bar acordingly to the current scroll state */
function scrollEvent() {
	var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
	var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
	var scrolled = (winScroll / height) * 100;
	document.getElementById("progress-bar").style.width = scrolled + "%";
}

/* Start to Scroll the Page in 5 Sec */
function startScroll() {
	if(numPressed == 0) {
		if(onMobile) {
			closeNavLeft();
		}
		numPressed++;
		snackbarCountdown();
		setTimeout('pageScroll()', 5000);
	}
}

/* Stop the Page from Scrolling */
function stopScroll(){
	if(numPressed == 1) {
		numPressed--;
		clearTimeout(scrolldelay);
	}
}

/* Scroll the Page */
function pageScroll() {
	if(onMobile) {
		window.scrollBy(0,scrollSpeed); 
		scrolldelay = setTimeout('pageScroll()',250);	
	} else {
		window.scrollBy(0,scrollSpeed); 
		scrolldelay = setTimeout('pageScroll()',150);	
	}
}

/* Scroll the Page if a sound abouth the Thrashold is detected */
function scrollOnSound(){
	if(checkboxAudio) {
		var icon = document.getElementById("soundIcon");
		if(volume > volumeThreshold) {
			icon.style.display= "inline";
			window.scrollBy(0,scrollSpeed);
		} else {
			icon.style.display= "none";
		}
	}
}









/*================================================================================*/
/* SOUND STUFF */
/*================================================================================*/

var volume = 0.0;

/* Try if it is possible to record Audio */
try {
	window.AudioContext = window.AudioContext || window.webkitAudioContext;
	window.audioContext = new AudioContext();
  } catch (e) {
	alert('Web Audio API not supported.');
  }
  
  /* Put variables in global scope to make them available to the browser console. */
  var constraints = window.constraints = {
	audio: true,
	video: false
  };
  
  /* Audio device is conected and Audio is recorded  */
  function handleSuccess(stream) {
	window.stream = stream;
	var soundMeter = window.soundMeter = new SoundMeter(window.audioContext);
	soundMeter.connectToSource(stream, function(e) {
	  if (e) {
		alert(e);
		return;
	  }
	  //Different listening intervalls on Mobile and on PC
	  if(onMobile) {
		  setInterval(function() {
			  volume = soundMeter.instant.toFixed(10);
			  scrollOnSound();
		  }, 250);
	  } else {
		  setInterval(function() {
			  volume = soundMeter.instant.toFixed(10);
			  scrollOnSound();
		  }, 150);
	  }
	});
  }
  
  /* Audio device is not detected or permission is denied */
  function handleError(error) {
	  alert("Sorry, no sound input was found.")
	  console.log('navigator.getUserMedia error: ', error);
  }
  
  /* get The audio device */
  navigator.mediaDevices.getUserMedia(constraints).
	  then(handleSuccess).catch(handleError);
  
  /* get the sound */
  function SoundMeter(context) {
	this.context = context;
	this.instant = 0.0;
	this.script = context.createScriptProcessor(2048, 1, 1);
	var that = this;
	this.script.onaudioprocess = function(event) {
	  var input = event.inputBuffer.getChannelData(0);
	  var i;
	  var sum = 0.0;
	  for (i = 0; i < input.length; ++i) {
		sum += input[i] * input[i];
	  }
	  that.instant = Math.sqrt(sum / input.length);
	};
  }
  
  /* Conecct the SoundMeter to the Audio device */
  SoundMeter.prototype.connectToSource = function(stream, callback) {
	console.log('SoundMeter connecting');
	try {
	  this.mic = this.context.createMediaStreamSource(stream);
	  this.mic.connect(this.script);
	  // necessary to make sample run, but should not be.
	  this.script.connect(this.context.destination);
	  if (typeof callback !== 'undefined') {
		callback(null);
	  }
	} catch (e) {
	  console.error(e);
	  if (typeof callback !== 'undefined') {
		callback(e);
	  }
	}
  };
  
  /* Stop recording sound and disconnect from audio device */
  SoundMeter.prototype.stop = function() {
	this.mic.disconnect();
	this.script.disconnect();
  };