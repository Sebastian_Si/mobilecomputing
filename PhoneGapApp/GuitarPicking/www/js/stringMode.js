/*The StringMode Buttons*/
var s1Pressed = false;
var s2Pressed = false;
var s3Pressed = false;
var s4Pressed = false;
var s5Pressed = false;
var s6Pressed = false;

/*Entry Point for the StringMode*/
var stringMode = {

    active : true,

    pressButon : function(button){
        if(stringRecordMode.active) {
            util.buttonBlink(button);
            switch(button) {
                case "s1":
                    s1Pressed = true;
                    break;
                case "s2":
                    s2Pressed = true;
                    break;
                case "s3":
                    s3Pressed = true;
                    break;
                case "s4":
                    s4Pressed = true;
                    break;
                case "s5":
                    s5Pressed = true;
                    break;
                case "s6":
                    s6Pressed = true;
                    break;
                default:
                    break;
            }
        }
    },

    selectPattern : function(patternName) {
        stringModePlayer.stop();
        currentPattern.setPattern(stringModePatterns.getPatternByName(patternName));
    },

    recordMode : function() {
        stringMode.active = false;
        stringRecordMode.active = true;
        document.getElementById("recordButton").style.backgroundColor = "red";
        document.getElementById("menuIcon").style.display = "none";
        document.getElementById("patternName").innerHTML = "Record Pattern";
        stringModePlayer.stop();
        app.turnStopButtonGreen();
        stringModeRecorder.resetRecorded();
    },

    start : function() {
        stringModePlayer.play();
    },

    stop : function() {
        stringModePlayer.stop();
    },

    loadPatterns : function() {
        var list = document.getElementById("patternList");
        var patterns = stringModePatterns.getAllPatterns();
        patterns.forEach(function(pattern) {
            var entry = document.createElement("li");
            entry.appendChild(document.createTextNode(pattern))
            list.appendChild(entry);
        });
    }
};

/*Entry Point for the StringRecordMode*/
var stringRecordMode = {

    active : false,

    tempPattern : [[]],

    recordMode : function() {
        stringMode.active = true;
        stringRecordMode.active = false;
        document.getElementById("recordButton").style.backgroundColor = "#111";
        document.getElementById("menuIcon").style.display = "inline-block";
        app.turnStopButtonGreen();
        if(this.tempPattern[0].length == 0){
            document.getElementById("patternName").innerHTML = "Select Pattern";
            currentPattern.clearPattern();
            return;
        }
        var safe = this.openSafeDialoge();
        if(safe) {
            var name = this.openNameDialogue();
            if(name == null) {
                document.getElementById("patternName").innerHTML = "Select Pattern";
                this.tempPattern = [[]];
                currentPattern.clearPattern();
                return;
            }
            document.getElementById("patternName").innerHTML = name;
            stringModePatterns.addPattern(name, this.tempPattern);
            currentPattern.setPattern(stringModePatterns.getPatternByName(name));
        } else {
            document.getElementById("patternName").innerHTML = "Select Pattern";
            this.tempPattern = [[]];
            currentPattern.clearPattern();
        }
    },

    openSafeDialoge : function() {
        return confirm("Do you want to safe the recorded pattern?");        
    },

    openNameDialogue : function() {
        return window.prompt("Please enter a pattern name!", "Custom Pattern");
    },

    start : function() {
        snackbar.snackbarCountdown();
        setTimeout('stringModeRecorder.startRecord()', 3500);
    },

    stop : function() {
        stringModeRecorder.stopRecord();
        this.tempPattern = stringModeRecorder.recordedPattern;
        stringModeRecorder.resetRecorded();
    }
};

/*The Pattern Player for the StringMode*/
var stringModePlayer = {

    count : 0,
     
    play : function() {
        nextElem = currentPattern.getNext();
        nextElem.forEach(element => { 
            if(vibratonActivated && connected) {
                this.buildPattern(element);
            }
            util.buttonBlink(this.getButton(element));
        });
        timeout = setTimeout(this.play.bind({getButton : this.getButton , play : this.play, buildPattern : this.buildPattern}), speed);
        if(vibratonActivated && connected) {
            glove.vibrate();
        }
    },

    stop : function() {
        clearTimeout(timeout);
        currentPattern.resetPattern();
        if(connected) {
            glove.stopVibrate();
        }
    },

    buildPattern : function(fingerNumber) {
        switch(fingerNumber) {
            case 1:
                glove.setVibratingPattern(true, false, false, false, false);
                break;
            case 2:
                glove.setVibratingPattern(true, false, false, false, false);
                break;
            case 3:
                glove.setVibratingPattern(true, false, false, false, false);
                break;
            case 4:
                glove.setVibratingPattern(false, true, false, false, false);
                break;
            case 5:
                glove.setVibratingPattern(false, false, true, false, false);
                break;
            case 6:
                glove.setVibratingPattern(false, false, false, true, false);
                break;
            default:
                return;
        }
    },

    getButton : function(buttonNumber) {
        switch(buttonNumber) {
            case 1:
                return "s1";
                break;
            case 2:
                return "s2";
                break;
            case 3:
                return "s3";
                break;
            case 4:
                return "s4";
                break;
            case 5:
                return "s5";
                break;
            case 6:
                return "s6";
                break;
            default:
                return "NOP"
        }
    }
};

/*The Pattern Recorder for the StringMode*/
var stringModeRecorder = {

    recordedPattern : [],

    recordPos : 0,
    
    startRecord : function() {
        this.clearButtonFlags();
        this.record();
    },

    record : function() {
        this.recordedPattern[this.recordPos] = this.getPressedButtons();
        this.recordPos++;
        timeout = setTimeout(this.record.bind({
            getPressedButtons : this.getPressedButtons,
            record : this.record,
            recordedPattern : this.recordedPattern,
            recordPos : this.recordPos, 
            clearButtonFlags : this.clearButtonFlags
        }), groundSpeed);
    },

    stopRecord : function() {
        clearTimeout(timeout);
    },

    resetRecorded : function() {
        this.recordedPattern = [];
        this.recordPos = 0;
    },

    getPressedButtons : function() {
        var pressedButtons = [];
        if(!(s1Pressed || s2Pressed || s3Pressed || s4Pressed || s5Pressed || s6Pressed)) {
            pressedButtons.push(0);
        } else {
            if(s1Pressed) pressedButtons.push(1);
            if(s2Pressed) pressedButtons.push(2);
            if(s3Pressed) pressedButtons.push(3);
            if(s4Pressed) pressedButtons.push(4);
            if(s5Pressed) pressedButtons.push(5);
            if(s6Pressed) pressedButtons.push(6);
        }
        this.clearButtonFlags();
        return pressedButtons;
    },

    clearButtonFlags : function() {
        s1Pressed = false;
        s2Pressed = false;
        s3Pressed = false;
        s4Pressed = false;
        s5Pressed = false;
        s6Pressed = false;
    }
};