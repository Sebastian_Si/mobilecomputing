/* Service/Characteristics for TECO Vibration Wearable.*/
var VIB_SERVICE             = "713D0000-503E-4C75-BA94-3148F18D941E";
var VIB_CHARACTERISTIC_1    = "713D0001-503E-4C75-BA94-3148F18D941E";
var VIB_CHARACTERISTIC_2    = "713D0002-503E-4C75-BA94-3148F18D941E";
var VIB_CHARACTERISTIC_3    = "713D0003-503E-4C75-BA94-3148F18D941E";


var scanTimeout;
var connected = false;
var connectedDevice;

/*================================================================================*/
/* BLE Stuff*/
/*================================================================================*/

function startBLEScan() {
	ble.isEnabled(bleEnabled, bleDisabled);
}

function bleDisabled() {
	alert("Please enable Bluetooth first.")
}

function bleEnabled() {
    
    snackbar.display("Scanning...");
    
	ble.scan([], 10, function(device) {
		if (device.name == "TECO Wearable 4") {
            snackbar.hide();
            snackbar.display("Found device. Connecting...");
			ble.stopScan(stopSuccess, stopFailure);
			clearTimeout(scanTimeout);
			ble.connect(device.id, connectSuccess, connectFailure);
            connected = true;
		} else {
			// Device is not one of our wearables. No action required.
		}
	}, function() {
        snackbar.hide();
		alert("Scan failed.");
    });
    
	scanTimeout = setTimeout(stopBLEScan, 10000);
}

function stopSuccess(){/*NOP*/}

function stopFailure() {
    snackbar.hide();
    alert("Scan failed.");
}

function stopBLEScan(){
    snackbar.hide();
	alert("Scan finished no device was found.");
}

function connectFailure(peripheral) {
    snackbar.hide();
    disconnectBLE();
}

function connectSuccess(device) {
    connectedDevice = device;
    snackbar.hide();
    snackbar.displayForTime("Connected!", 1.5);

    glove.resetVibratingPattern();
    glove.resetProperties();

    document.getElementById("connectButton").innerHTML = "Disconnect";
    document.getElementById("connectButton").style.color = "red";
}

function writeDone() {
	//NOP
}

function writeFailure() {
	alert("There was an error while sending data to the glove.")
}

function disconnectBLE() {
    ble.disconnect(connectedDevice.id, disconnectSuccess, disconnectFailure);
}

function disconnectSuccess() {
    snackbar.displayForTime("Disconnected", 1.5);
    document.getElementById("connectButton").innerHTML = "Scan and Connect";
    document.getElementById("connectButton").style.color = "green";
    connected = false;
    glove.resetVibratingPattern();
    app.stop();
    app.turnVibrationOff();
    app.turnStopButtonGreen();
}

function disconnectFailure() {
    alert("Unexpected dissconect");
    document.getElementById("connectButton").innerHTML = "Scan and Connect";
    document.getElementById("connectButton").style.color = "green";
    connected = false;
    glove.resetVibratingPattern();
    app.stop();
    app.turnVibrationOff();
    app.turnStopButtonGreen();
}



/*The connected Glove*/
var glove = {

    maxUpdates :  6,

    numVibrationEngines :  4,

    vibratingPattern : new Uint8Array(4),

    resetProperties : function() {
        this.maxUpdates = 6;
        this.numVibrationEngines = 4;
    },

    resetVibratingPattern : function() {
        this.vibratingPattern[0] = 0x00;
        this.vibratingPattern[1] = 0x00;
        this.vibratingPattern[2] = 0x00;
        this.vibratingPattern[3] = 0x00;
    },

    vibrate : function() {
        if(connected) {
            ble.writeWithoutResponse(connectedDevice.id, VIB_SERVICE, VIB_CHARACTERISTIC_3, this.vibratingPattern.buffer, writeDone, writeFailure);
            this.resetVibratingPattern();
        } else {
            alert("No connection to glove. Vibration will be turned off. Please reconnect ");
            disconnectBLE();
        }
    },

    setVibratingPattern : function(thumb, indexFinger, middleFinger, ringFinger, pinkieFinger) { 
        if(this.numVibrationEngines == 4) {
            //output of thump to pinkieFinger on glove with 4 motors
            if(thumb) this.vibratingPattern[0] = 0xFF;
            if(ringFinger) this.vibratingPattern[1] = 0xFF;
            if(middleFinger) this.vibratingPattern[2] = 0xFF;
            if(indexFinger) this.vibratingPattern[3] = 0xFF;
        } else if(this.numVibrationEngines == 5){
            //for glove with 5 motors use this:
            if(pinkieFinger) this.vibratingPattern[0] = 0xFF;
            if(ringFinger) this.vibratingPattern[1] = 0xFF;
            if(middleFinger) this.vibratingPattern[2] = 0xFF;
            if(indexFinger) this.vibratingPattern[3] = 0xFF;
            if(thumb) this.vibratingPattern[4] = 0xFF;
        } else {
            app.turnVibrationOff();
            alert("There are to less vibration engines on this glove.")
        }
    },

    stopVibrate : function() {
        this.resetVibratingPattern();
        ble.writeWithoutResponse(connectedDevice.id, VIB_SERVICE, VIB_CHARACTERISTIC_3, this.vibratingPattern.buffer, writeDone, writeFailure);
    }
}


