/*Variables to Controll the UI*/
var buttonRdyToStart = true;
var groundSpeed = 500;
var speed;
var vibratonActivated = false;
var timeout;
var menuOpen = false;
var patternMenuOpen = false;
var connectedBeforePause = false;
var snackbarTimeout;

// Start listening for deviceready event.
document.addEventListener('deviceready', onDeviceReady, false);

/*Set up on Resume/pause and Volume button stuff*/
function onDeviceReady() {
    document.addEventListener("resume", onResume, false);
    document.addEventListener("pause", onPause, false);
    document.addEventListener("volumedownbutton", onVolumeDownKeyDown, false);
    document.addEventListener("volumeupbutton", onVolumeUpKeyDown, false);
}

function onResume() {
    if(connectedBeforePause) {
        startBLEScan();
    }
}

function onPause(){
    if(connected){
        disconnectBLE();
        connectedBeforePause = true;
    } else {
        connectedBeforePause = false;
    }
}

function onVolumeDownKeyDown() {
    var sliderValue = $("#speedSlider").val();
    $("#speedSlider").val(sliderValue - 10).slider("refresh");
    app.setSpeed();
}

function onVolumeUpKeyDown() {
    var sliderValue = $("#speedSlider").val();
    var newSliderValue = parseInt(sliderValue) + parseInt(10);
    $("#speedSlider").val((newSliderValue)).slider("refresh");
    app.setSpeed();
}

/*The Main View of the App and its functionality*/
var app = {
    
    setSpeed : function() {
        var sliderValue = $("#speedSlider").val()
        speed = groundSpeed * (100 / sliderValue);
        if(vibratonActivated) {
            if(speed <= (1000 / glove.maxUpdates)) {
                alert("Speed to high for glove. Vibration will be disabled.")
                this.turnVibrationOff();
                glove.stopVibrate();
            }
        }
        
    },

    toggleVibration : function() {
        if(!vibratonActivated) {
            if(connected) {
                vibratonActivated = true;
            } else {
                alert("Please connect to a device bevore turning vibration on.")
                this.turnVibrationOff();
            }
        } else {
            vibratonActivated = false;
            if(connected) {
                glove.stopVibrate();
            }
        }
    },

    turnVibrationOff : function() {
        $("#vibrationSwitch").val('off').slider('refresh');
        vibratonActivated = false;
    },

    pressConnectButton : function() {
        if(connected) {
            disconnectBLE();
        } else {
            startBLEScan();
        }
    },

    openMenue : function() {
        if(!menuOpen) {
            document.getElementById("menu").style.width = "100%";
            menuOpen = true;
        } else {
            this.closeMenue();
        }
    },

    closeMenue : function() {
        document.getElementById("menu").style.width = "0%";
        menuOpen = false;
    },

    openPatternMenu : function() {
        if(stringRecordMode.active || handRecordMode.active) return;
        document.getElementById("patternMenu").style.height = "100%";
        document.getElementById("patternMenu").style.paddingTop = "100px";
        if(!patternMenuOpen) {
            if(stringMode.active) {
                stringMode.loadPatterns();
            } else if(handMode.active) {
                handMode.loadPatterns();
            } else {
                console.log("Error, openPatternMenue call in recordMode")
            }
            patternMenuOpen = true;
        } else {
            this.closePatternMenu();
        }
    },

    closePatternMenu : function() {
        document.getElementById("patternMenu").style.height = "0%";
        var ul = document.getElementById("patternList");
        while(ul.firstChild) ul.removeChild(ul.firstChild);
        document.getElementById("patternMenu").style.paddingTop = "0px";
        patternMenuOpen = false;
    },

    selectPatternInMenu : function(event) {
        var target = util.getListTarget(event);
        this.selectPattern(target.innerHTML);
        this.closePatternMenu();
    },

    switchMode : function() {
        if(stringMode.active) {
            this.stop();
            this.turnStopButtonGreen();
            currentPattern.clearPattern();
            document.getElementById("patternName").innerHTML = "Select Pattern";
            stringMode.active = false;
            handMode.active = true;
            document.getElementById("stringModeContent").style.display = "none";
            document.getElementById("handModeContent").style.display = "grid";
        } else if(handMode.active) {
            this.stop();
            this.turnStopButtonGreen();
            currentPattern.clearPattern();
            document.getElementById("patternName").innerHTML = "Select Pattern";
            stringMode.active = true;
            handMode.active = false;
            document.getElementById("stringModeContent").style.display = "grid";
            document.getElementById("handModeContent").style.display = "none";
        } else {
            console.log("Error, Switch mode in record mode");
        }
    },

    clickStartStopButton(){
        if(buttonRdyToStart){
            this.turnStopButtonRed();
            this.start();
        } else {
            this.turnStopButtonGreen();
            this.stop();
        }
    },

    turnStopButtonGreen : function() {
        document.getElementById("startStopButton").style.backgroundColor = "green";
        document.getElementById("startStopImg").src = "res/icon/play.png";
        buttonRdyToStart = true;
    },

    turnStopButtonRed : function() {
        document.getElementById("startStopButton").style.backgroundColor = "red";
        document.getElementById("startStopImg").src = "res/icon/stop.png";
        buttonRdyToStart = false;
    },

    selectPattern : function(patternName) {
        if(stringMode.active) {
            stringMode.selectPattern(patternName);
        } else if(handMode.active) {
            handMode.selectPattern(patternName);
        } else if(stringRecordMode.active) {
            console.log("Error, selectPattern call in stringRecordMode");
        } else if(handRecordMode.active) {
            console.log("Error, selectPattern call in handRecordMode");
        } else {
            console.log("Error, selectPattern call but no mode active");
        }
        document.getElementById("patternName").innerHTML = patternName;
    },

    recordMode : function() {
        if(stringMode.active) {
            stringMode.recordMode();
        } else if(handMode.active) {
            handMode.recordMode();
        } else if(stringRecordMode.active) {
            stringRecordMode.recordMode();
        } else if(handRecordMode.active) {
            handRecordMode.recordMode();
        } else {
            console.log("Error: no mode active");
        }
    },

    start : function() {
        if(document.getElementById("patternName").innerHTML == "Select Pattern") {
            alert("No pattern selected. Please select a pattern");
            this.turnStopButtonGreen();
            this.stop();
            return;
        } 
        if(stringMode.active) {
            stringMode.start();
        } else if(handMode.active) {
            handMode.start();
        } else if(stringRecordMode.active) {
            stringRecordMode.start();
        } else if(handRecordMode.active) {
            handRecordMode.start();
        } else {
            console.log("Error: no mode active");
        }
    },

    stop : function() {
        if(stringMode.active) {
            stringMode.stop();
        } else if(handMode.active) {
            handMode.stop();
        } else if(stringRecordMode.active) {
            stringRecordMode.stop();
        } else if(handRecordMode.active) {
            handRecordMode.stop();
        } else {
            console.log("Error: no mode active");
        }
    },


};

/*The Snackbar on the botoom of the Page and its functionality*/
var snackbar = {


    display : function(content) {
        var x = document.getElementById("snackbar");
        x.innerHTML = content;
        x.className = "show";
    },

    displayForTime : function(content, seconds) {
        var x = document.getElementById("snackbar");
        x.innerHTML = content;
        x.className = "show";
        snackbarTimeout = setTimeout(function(){ x.className = x.className.replace("show", ""); }, seconds * 1000);
    },

    hide : function() {
        //clearTimeout(snackbarTimeout);
        var x = document.getElementById("snackbar");
        x.className = x.className.replace("show", "");
    },

    snackbarCountdown : function() {
        var x = document.getElementById("snackbar");
        x.innerHTML = 3;
        x.className = "show";
        setTimeout(function(){x.innerHTML = 2;},1000);
        setTimeout(function(){x.innerHTML = 1;},2000);
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3500);
    }
}


var util = {
    buttonBlink : function(button) {
        if(button == "NOP") {
            return;
        }
        document.getElementById(button).style.backgroundColor = "red";
        setTimeout(function(){document.getElementById(button).style.backgroundColor = "#666"}, 200);
    },

    getListTarget : function(e) {
        e = e || window.event;
        return e.target || e.srcElement;
    }
};

/*The currently selected Pattern*/
var currentPattern = {

    pattern : [[]],

    pos : 0,

    setPattern : function(toSet) {
        this.resetPattern();
        this.pattern = toSet;
    },

    clearPattern : function() {
        this.pattern = [[]];
        this.resetPattern();
    },

    resetPattern : function() {
        this.pos = 0;
    },

    getNext : function() {
        if(this.pos == this.pattern.length) this.pos = 0;
        return this.pattern[this.pos++];
    }
};


