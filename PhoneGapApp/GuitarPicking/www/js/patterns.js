/* Initalizes the StringModePatterns and the HandModePatterns*/
function initPatterns() {
    stringModePatterns.init();
    handModePatterns.init();
}

/*The Patterns for the StringMode*/
var stringModePatterns = {

    patterns : {},

    init : function() {
        this.patterns["Four Quarters Clock"] = [[1,6],[4],[5],[4]];
        this.patterns["Three Quarters Clock"] = [[1,6],[4],[5],[4],[1,6],[4]];
        this.patterns["Easy Example"] = [[4],[5],[5,6],[4]];
    },
    
    getPatternByName : function(patternName) {
        return this.patterns[patternName];
    },

    addPattern : function(name, pattern) {
        this.patterns[name] = pattern;
    },

    getAllPatterns : function() {
        var out = [];
        for(var key in this.patterns) {
            out.push(key);
        }
        return out;
    },    
};

/*The Patterns for the HandMode*/
var handModePatterns = {

    patterns : {},

    init : function() {
        this.patterns["Four Quarters Clock"] = [[1,4],[2],[3],[2]];
        this.patterns["Three Quarters Clock"] = [[1,4],[2],[3],[2],[1,4],[2]];
        this.patterns["Easy Example"] = [[2],[3],[3,4],[2]];
        this.patterns["Dust In The Wind"] = [[1,4],[2],[3],[2],[4],[2],[3]];
    },

    getPatternByName : function(patternName) {
        return this.patterns[patternName];
    },

    addPattern : function(name, pattern) {
        this.patterns[name] = pattern;
    },

    getAllPatterns : function() {
        var out = [];
        for(var key in this.patterns) {
            out.push(key);
        }
        return out;
    },    
};