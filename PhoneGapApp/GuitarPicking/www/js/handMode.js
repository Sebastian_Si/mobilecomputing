/* The HandMode Buttons*/
var h1Pressed = false;
var h2Pressed = false;
var h3Pressed = false;
var h4Pressed = false;
var h5Pressed = false;

/*Entry Point for the HandMode*/
var handMode = {

    active : false,

    pressButon : function(button){
        if(handRecordMode.active) {
            util.buttonBlink(button);
            switch(button) {
                case "h1":
                    h1Pressed = true;
                    break;
                case "h2":
                    h2Pressed = true;
                    break;
                case "h3":
                    h3Pressed = true;
                    break;
                case "h4":
                    h4Pressed = true;
                    break;
                case "h5":
                    h5Pressed = true;
                    break;
                default:
                    break;
            }
        }
    },

    selectPattern : function(patternName) {
        handModePlayer.stop();
        currentPattern.setPattern(handModePatterns.getPatternByName(patternName));
    },

    recordMode : function() {
        handMode.active = false;
        handRecordMode.active = true;
        document.getElementById("recordButton").style.backgroundColor = "red";
        document.getElementById("menuIcon").style.display = "none";
        document.getElementById("patternName").innerHTML = "Record Pattern";
        handModePlayer.stop();
        app.turnStopButtonGreen();
        handModeRecorder.resetRecorded();
    },

    start : function() {
        handModePlayer.play();
    },

    stop : function() {
        handModePlayer.stop();
    },

    loadPatterns : function() {
        var list = document.getElementById("patternList");
        var patterns = handModePatterns.getAllPatterns();
        patterns.forEach(function(pattern) {
            var entry = document.createElement("li");
            entry.appendChild(document.createTextNode(pattern))
            list.appendChild(entry);
        });
    }
};

/*Entry Point for the HandRecordMode*/
var handRecordMode = {

    active : false,

    tempPattern : [[]],

    recordMode : function() {
        handMode.active = true;
        handRecordMode.active = false;
        document.getElementById("recordButton").style.backgroundColor = "#111";
        document.getElementById("menuIcon").style.display = "inline-block";
        app.turnStopButtonGreen();
        if(this.tempPattern[0].length == 0){
            document.getElementById("patternName").innerHTML = "Select Pattern";
            currentPattern.clearPattern();
            return;
        }
        var safe = this.openSafeDialoge();
        if(safe) {
            var name = this.openNameDialogue();
            if(name == null) {
                document.getElementById("patternName").innerHTML = "Select Pattern";
                this.tempPattern = [[]];
                currentPattern.clearPattern();
                return;
            }
            document.getElementById("patternName").innerHTML = name;
            handModePatterns.addPattern(name, this.tempPattern);
            currentPattern.setPattern(handModePatterns.getPatternByName(name));
        } else {
            document.getElementById("patternName").innerHTML = "Select Pattern";
            this.tempPattern = [[]];
            currentPattern.clearPattern();
        }
    },

    openSafeDialoge : function() {
        return confirm("Do you want to safe the recorded pattern?");        
    },

    openNameDialogue : function() {
        return window.prompt("Please enter a pattern name!", "Custom pattern");
    },

    start : function() {
        snackbar.snackbarCountdown();
        setTimeout('handModeRecorder.startRecord()', 3500);
    },

    stop : function() {
        handModeRecorder.stopRecord();
        this.tempPattern = handModeRecorder.recordedPattern;
        handModeRecorder.resetRecorded();
    }
};

/*The Pattern Player for the HandMode*/
var handModePlayer = {

    count : 0,

    play : function() {
        nextElem = currentPattern.getNext();
        nextElem.forEach(element => {
            if(vibratonActivated && connected) {
                this.buildPattern(element);
            }
            util.buttonBlink(this.getButton(element));
        });
        timeout = setTimeout(this.play.bind({getButton : this.getButton , play : this.play, buildPattern : this.buildPattern}), speed);
        if(vibratonActivated && connected) {
            glove.vibrate();
        }
    },

    stop : function() {
        clearTimeout(timeout);
        currentPattern.resetPattern();
        if(connected) {
            glove.stopVibrate();
        }
    },

    buildPattern : function(fingerNumber) {
        switch(fingerNumber){
            case 1:
                glove.setVibratingPattern(true, false, false, false, false);
                break;
            case 2:
                glove.setVibratingPattern(false, true, false, false, false);
                break;
            case 3:
                glove.setVibratingPattern(false, false, true, false, false);
                break;
            case 4:
                glove.setVibratingPattern(false, false, false, true, false);
                break;
            case 5:
                glove.setVibratingPattern(false, false, false, false, true);
                break;
            default:
                return;
        }
    },

    getButton : function(buttonNumber) {
        switch(buttonNumber) {
            case 1:
                return "h1";
                break;
            case 2:
                return "h2";
                break;
            case 3:
                return "h3";
                break;
            case 4:
                return "h4";
                break;
            case 5:
                return "h5";
                break;
            default:
                return "NOP"
        }
    }
};

/*The Pattern Recorder for the HandMode*/
var handModeRecorder = {

    recordedPattern : [],

    recordPos : 0,

    startRecord : function() {
        this.clearButtonFlags();
        this.record();
    },

    record : function() {
        this.recordedPattern[this.recordPos] = this.getPressedButtons();
        this.recordPos++;
        timeout = setTimeout(this.record.bind({
            getPressedButtons : this.getPressedButtons,
            record : this.record,
            recordedPattern : this.recordedPattern,
            recordPos : this.recordPos, 
            clearButtonFlags : this.clearButtonFlags
        }), groundSpeed);
    },

    stopRecord : function() {
        clearTimeout(timeout);
    },

    resetRecorded : function() {
        this.recordedPattern = [];
        this.recordPos = 0;
    },

    getPressedButtons : function() {
        var pressedButtons = [];
        if(!(h1Pressed || h2Pressed || h3Pressed || h4Pressed || h5Pressed)) {
            pressedButtons.push(0);
        } else {
            if(h1Pressed) pressedButtons.push(1);
            if(h2Pressed) pressedButtons.push(2);
            if(h3Pressed) pressedButtons.push(3);
            if(h4Pressed) pressedButtons.push(4);
            if(h5Pressed) pressedButtons.push(5);
        }
        this.clearButtonFlags();
        return pressedButtons;
    },

    clearButtonFlags : function() {
        h1Pressed = false;
        h2Pressed = false;
        h3Pressed = false;
        h4Pressed = false;
        h5Pressed = false;
    }
};